source ./gitapi.sh
echo "####################################################"
echo "1)clone Repo from BitBucket"
echo "2)Create New Maven Multi Module Project"
echo "3)Update the Maven Project Release Version"
echo "4)Create Development branch"
echo "5)Delete Branch"
echo "6)Merge Branches to Master"
echo "7)Create Tag"
echo "#####################################################"
echo "Enter Your Choice"
read NUM

case $NUM in
"1")
echo "Starting"
git_clone
echo "Ending"
;;
"2")
git_clone
create_maven_multi_module
git_push
;;
"3")
git_clone
create_version
;;
"4")
git_clone
create_branch
;;
"5")
git_clone
delete_branch
;;
"6")
git_clone
git_merge
;;
"7")
git_clone
git_tag
;;
*)
echo "Invalid Choice"
;;
esac
