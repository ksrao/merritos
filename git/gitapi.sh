#!/bin/bash
echo "Enter Repo to be cloned"
read REPOSITORY
update()
{
#Yum Update
yum update -y
yum install wget -y
}
install_jdk()
{
#Installing Java
wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm -P /root
rpm -ivh /root/jdk-8u131-linux-x64.rpm
}
install_maven()
{
#Installing Maven 
wget http://mirror.olnevhost.net/pub/apache/maven/maven-3/3.0.5/binaries/apache-maven-3.0.5-bin.tar.gz
tar -xvzf apache-maven-3.0.5-bin.tar.gz
mkdir /usr/local/apache-maven
mv apache-maven-3.0.5  /usr/local/apache-maven
touch .profile
echo "export M2_HOME=/usr/local/apache-maven/apache-maven-3.0.5" >> .profile
echo "export M2=$M2_HOME/bin" >> .profile
echo "export PATH=$M2:$PATH" >> .profile
source ~/.profile
}
git_clone()
{
if [ -d "$REPOSITORY" ]; then
echo "$REPOSITORY already exist so perform clean check out"
rm -rf $REPOSITORY
fi
git clone https://kanisetty_srinivas@bitbucket.org/kanisetty_srinivas/$REPOSITORY.git
}
create_maven_multi_module()
{
#Creating New Maven multi module Project
mvn archetype:generate -DgroupId=com.example -DartifactId=FSI -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
cd FSI
rm -rf src
sed -i -e 's/jar/pom/g' pom.xml

mvn archetype:generate -DgroupId=com.example -DartifactId=FSILib     -DarchetypeArtifactId=maven-archetype-quickstart     -DinteractiveMode=false

mvn archetype:generate -DgroupId=com.example -DartifactId=FSIHelpwar -DarchetypeArtifactId=maven-archetype-quickstart     -DinteractiveMode=false

mvn archetype:generate -DgroupId=com.example -DartifactId=FSIWar     -DarchetypeArtifactId=maven-archetype-webapp         -DinteractiveMode=false

mvn archetype:generate -DgroupId=com.example -DartifactId=FSIEar     -DarchetypeArtifactId=maven-archetype-quickstart     -DinteractiveMode=false
mvn archetype:generate -DgroupId=com.example -DartifactId=FSIBatch   -DarchetypeArtifactId=maven-archetype-quickstart     -DinteractiveMode=false
mvn archetype:generate -DgroupId=com.example -DartifactId=FSIStatic  -DarchetypeArtifactId=maven-archetype-quickstart     -DinteractiveMode=false
}
git_push()
{
echo `pwd`
cd ..
cp -R FSI $REPOSITORY
cd $REPOSITORY
git add .
git commit -m "New Repo"
git push
}
create_version()
{
echo "################################################################"
cd $REPOSITORY
pwd
inside_git_repo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"
echo "*$inside_git_repo*"
if [ ! "$inside_git_repo" ]; then
echo "Not in git repo"
else
git branch -a
echo "To which branch you would like to setup version???"
read stb
git checkout $stb
git branch
echo "setting up version to the parent and it's child projects"
echo "Please Enter Project Version in the format of Major.Minor.Fix(use TAB in between?"
read Major Minor Fix
cd ..
proj=`ls -l $REPOSITORY | awk '{print $9}'| head -n 2`
echo "###########################################"
echo $proj
cd $REPOSITORY
cd $proj
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$Major.$Minor.$Fix.0-SNAPSHOT
cd ..
git config --global user.name "srinivas"
git config --global user.email kanisetty.srinivas@gmail.com
git add .
git commit -m "New Version($Major.$Minor.$Fix) Updated"
git push
fi
}

create_branch()
{
cd $REPOSITORY
git checkout master

echo "Enter the branch name which you would like to create"
read bname
git branch $bname
git checkout $bname
git push --set-upstream origin $bname
 
}
delete_branch()
{
cd $REPOSITORY
echo "Enter branch which you want to delete"
echo "branches available"
git branch -a
read bname
git push origin --delete $bname
#git branch -d <branch_name>
}
git_merge()
{
cd $REPOSITORY
echo "Enter branch to be merged"
read bname
git status
git branch
git checkout $bname
git branch
git status
git checkout master
git status
git merge $bname
git status
git add .
git commit -m "merge"
git push
}
git_tag()
{
echo "Tag"
cd $REPOSITORY
git tag -a 'Release_1_0' -m 'Tagged basic string operation code' HEAD
git push origin tag Release_1_0
git tag -l
#git checkout tags/<tag> -b <branch>
}
release_version()
{
#Release
echo "Hello, Do you want to Release? If Yes,Please Update the Version(Major.Minor.Fix)"
read Major Minor Fix
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$Major.$Minor.$Fix.0
}
