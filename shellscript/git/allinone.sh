#!/bin/bash
echo "Enter Repo to be cloned"
read REPOSITORY
git_clone()
{
if [ -d "$REPOSITORY" ]; then
echo "$REPOSITORY already exist so perform clean check out"
rm -rf $REPOSITORY
fi
git clone https://kanisetty_srinivas@bitbucket.org/kanisetty_srinivas/$REPOSITORY.git
}
create_version()
{
echo "################################################################"
cd $REPOSITORY
pwd
inside_git_repo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"
echo "*$inside_git_repo*"
if [ ! "$inside_git_repo" ]; then
echo "Not in git repo"
else
git branch -a
echo "To which branch you would like to setup version???"
read stb
git checkout $stb
git branch
echo "setting up version to the parent and it's child projects"
echo "Please Enter Project Version in the format of Major.Minor.Fix(use TAB in between?"
read Major Minor Fix
cd ..
proj=`ls -l $REPOSITORY | awk '{print $9}'| head -n 2`
echo "###########################################"
echo $proj
cd $REPOSITORY
cd $proj
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$Major.$Minor.$Fix.0-SNAPSHOT
cd ..
git config --global user.name "srinivas"
git config --global user.email kanisetty.srinivas@gmail.com
git add .
git commit -m "New Version($Major.$Minor.$Fix) Updated"
git push
fi
}

create_branch()
{
cd $REPOSITORY
git checkout master

#echo "Enter the branch name which you would like to create"
bname=$Major.$Minor.$Fix.0-SNAPSHOT
git branch $bname
git checkout $bname
git push --set-upstream origin $bname
}
delete_branch()
{
cd $REPOSITORY
echo "Enter branch which you want to delete"
echo "branches available"
git branch -a
read bname
git push origin --delete $bname
}
git_merge()
{
cd $REPOSITORY
echo "Enter branch to be merged"
read bname
git status
git branch
git checkout $bname
git branch
git status
git checkout master
git status
git merge $bname
git status
git add .
git commit -m "merge"
git push
}
git_tag()
{
echo "Tag"
cd $REPOSITORY
git tag -a 'Release_1_0' -m 'Tagged basic string operation code' HEAD
git push origin tag Release_1_0
git tag -l
}

git_clone
create_version
create_branch
git_merge
git_tag
