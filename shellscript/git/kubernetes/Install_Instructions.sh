#!/bin/bash

#############################################################################
#Pre-Steps:
#Make sure all the machines you going to use kubernetes should disable SElinux and swap and #Enable br_netfilter.
#Disable SELinux 
setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux

#Disable Swap
swapoff �a
#Enable Br-netfilter
modprobe br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

#Installing dependencies
#Before start to kubernetes, we must install the docker tools, to install use following commands,
#yum install -y yum-utils device-mapper-persistent-data lvm2

#Now, add the Docker-ce repository with the command:

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
#Install Docker-ce with the command:
yum install -y docker-ce
#Once that completes, start and enable the Docker service with the commands
systemctl start docker
systemctl enable docker


cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF



#Installing Kubernetes
#Installing the necessary components for Kubernetes is simple. Again, what we�re going to install below must be installed on all machines that will be joining the cluster.
#First add a repository by creating the file

yum update �y && yum install -y kubelet kubeadm kubectl
#sed -i 's/cgroup-driver=systemd/cgroup-driver=cgroupfs/g' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
#Restart the systemd daemon and the kubelet service with the commands:
systemctl daemon-reload
systemctl restart kubelet

ip=$(hostname -I | awk '{print $1}')

echo "$ip master" >> /etc/hosts



#Initialize your master
#With everything installed, go to the machine that will serve as the Kubernetes master and issue the command:
kubeadm init --node-name master




useradd ksrao
passwd ksrao
sed -i 's/# %wheel        ALL=(ALL)       ALL/ %wheel        ALL=(ALL)       ALL/g' /etc/sudoers
usermod -aG wheel ksrao
su ksrao -
groups
su ksrao
cd ~
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl get nodes


export kubever=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
kubectl get nodes


